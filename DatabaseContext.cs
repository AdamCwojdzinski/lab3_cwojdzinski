using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace example_dotnet
{
    public class DatabaseContext
    {
        private readonly string connectionString;
        private const string QueryAllPerson = "Select * from persons";
        private const string QueryPersonById = "Select * from persons where PersonId = @ID";
        private const string QueryAddNewPerson = "Insert Into persons(PersonId, FirstName, LastName) Values (@ID, @FirstName, @LastName)";
        private const string QueryUpdatePerson = "Update persons Set FirstName = @FirstName, LastName = @LastName where PersonId = @ID";
        private const string QueryDeletePerson = "Delete from persons where PersonId = @ID";

        public DatabaseContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IEnumerable<Person> GetPeople()
        {
            var people = new List<Person>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(QueryAllPerson, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    people.Add(new Person
                    {
                        PersonId = Convert.ToInt32(reader["PersonId"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString()

                    });
                }
                reader.Close();
            }

            return people;
        }

        public Person GetPersonById(int id)
        {
            var people = new Person();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(QueryPersonById, connection);
                command.Parameters.AddWithValue("@ID", id);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    people = (new Person
                    {
                        PersonId = Convert.ToInt32(reader["PersonId"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString()

                    });
                } 
                reader.Close();
            }

            return people;
        }

        public void AddNewPerson(Person person)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(QueryAddNewPerson, connection);
                command.Parameters.AddWithValue("@ID", person.PersonId);
                command.Parameters.AddWithValue("FirstName", person.FirstName);
                command.Parameters.AddWithValue("LastName", person.LastName);
                connection.Open();
                command.ExecuteNonQuery();
            }

        }
        public void UpdatePerson(Person person)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(QueryUpdatePerson, connection);
                command.Parameters.AddWithValue("@ID", person.PersonId);
                command.Parameters.AddWithValue("@FirstName", person.FirstName);
                command.Parameters.AddWithValue("@LastName", person.LastName);
                connection.Open();
                command.ExecuteNonQuery();
            }

        }
        public void DeletePerson(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(QueryPersonById, connection);
                command.Parameters.AddWithValue("@ID", id);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

    }
}