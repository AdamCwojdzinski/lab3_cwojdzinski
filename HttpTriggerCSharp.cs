using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;


namespace example_dotnet
{
    public static class HttpTriggerCSharp
    {
        [FunctionName("GetAllPerson")]
        public static async Task<IActionResult> GetAllPerson(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request get All Person.");
            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PersonDb");
                log.LogInformation(connectionString);
                var db = new DatabaseContext(connectionString);
                var people = db.GetPeople();
                return new JsonResult(people);
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }

        [FunctionName("GetPersonById")]
        public static async Task<IActionResult> GetPersonById(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "{id}")] HttpRequest req, 
            ILogger log, Int32 id)
        {
            log.LogInformation("C# HTTP trigger function processed a request get PersonById.");
            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PersonDb");
                log.LogInformation(connectionString);
                var db = new DatabaseContext(connectionString);
                var person = db.GetPersonById(id);
                return new JsonResult(person);
            }
            catch(Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }

        [FunctionName("CreateNewPerson")]
        public static async Task<IActionResult> CreateNewPerson(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "person")] HttpRequest req, 
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request post new Person");
            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PersonDb");
                log.LogInformation(connectionString);
                var db = new DatabaseContext(connectionString);
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                var inputData = JsonConvert.DeserializeObject<Person>(requestBody);
                var person = new Person {
                    PersonId = inputData.PersonId,
                    FirstName = inputData.FirstName,
                    LastName = inputData.LastName
                };
                db.AddNewPerson(person);
                return new JsonResult(person);
            }
            catch(Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }

        [FunctionName("UpdatePerson")]
        public static async Task<IActionResult> UpdatePerson(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "{id}")] HttpRequest req, 
            ILogger log, Int32 id)
        {
            log.LogInformation("C# HTTP trigger function processed a request Update Person");
            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PersonDb");
                log.LogInformation(connectionString);
                var db = new DatabaseContext(connectionString);
                var person = db.GetPersonById(id);
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                var updateData = JsonConvert.DeserializeObject<Person>(requestBody);
                var updatePerson = new Person {
                    PersonId = updateData.PersonId,
                    FirstName = updateData.FirstName,
                    LastName = updateData.LastName
                };
                db.UpdatePerson(updatePerson);
                return new OkResult();
            }
            catch(Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }

         [FunctionName("DeletePerson")]
        public static async Task<IActionResult> DeletePerson(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "{id}")]HttpRequest req, 
            ILogger log, Int32 id)
        {
            log.LogInformation("C# HTTP trigger function processed a request Delete Person");
            try
            {
                string connectionString = Environment.GetEnvironmentVariable("PersonDb");
                log.LogInformation(connectionString);
                var db = new DatabaseContext(connectionString);
                db.DeletePerson(id);
                return new OkResult();
            }
            catch(Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new JsonResult(ex);
            }
        }
    }
}
